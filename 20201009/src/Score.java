import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class Score {
	public static void main(String[] args) throws IOException {
		// 读取配置文件
		Properties ppt = new Properties();
		ppt.load(new FileInputStream("make/total.properties"));
		// 原始题目经验值
		double total_before = Integer.parseInt(ppt.getProperty("before"));
		double total_base = Integer.parseInt(ppt.getProperty("base"));
		double total_test = Integer.parseInt(ppt.getProperty("test"));
		double total_program = Integer.parseInt(ppt.getProperty("program"));
		double total_add = Integer.parseInt(ppt.getProperty("add"));

		// 初始化每个人的经验值
		int before = 0;
		int base = 0;
		int test = 0;
		int program = 0;
		int add = 0;
        try{
		/// 读取本地网页文件
		File small = new File("small.html");// 本地的一个html文件
		Document smallDoc = Jsoup.parse(small, "UTF-8",
				" https://www.mosoteach.cn/web/index.php?c=interaction&m=index&clazz_course_id=8AF72060-4C93-11EA-9C7F-98039B1848C6");
		File all = new File("all.html");
		Document allDoc = Jsoup.parse(all, "UTF-8",
				" https://www.mosoteach.cn/web/index.php?c=interaction&m=index&clazz_course_id=9E603F91-4AF8-11EA-9C7F-98039B1848C6");// 查看原帖

		// 小网页经验值计算
		// 小网页总共资源几大类的统计
		int rowslength = smallDoc.select("div[class=interaction-row]").size();
		// 全部class的长度
		// 统计每一类里资源块的个数，利用JSoup在html提取
		for (int i = 0; i < rowslength; i++) {
			//int rowssize = smalldoc.select("div[class=interaction-row]").get(i).select("span").size();

			String rows = smallDoc.select("div[class=interaction-row]").get(i).select("span").get(1).text();
			int spansize = smallDoc.select("div[class=interaction-row]").get(i).select("span").size();
			// 块状资源根据名称进行同类经验值的累计
			if (rows.indexOf("课前自测") != -1) {
				String selfget = smallDoc.select("div[class=interaction-row]").get(i).select("span").get(spansize - 1)
						.text();
				before += isNum(selfget);
			}
			else if (rows.indexOf("课堂小测") != -1) {
				String testget = smallDoc.select("div[class=interaction-row]").get(i).select("span").get(spansize - 1)
						.text();
				test += isNum(testget);
			}
			else if (rows.indexOf("课堂完成") != -1) {
				String baseget = smallDoc.select("div[class=interaction-row]").get(i).select("span").get(spansize - 1)
						.text();
				base += (isNum(baseget));

			}
			else if (rows.indexOf("编程题") != -1) {
				String proget = smallDoc.select("div[class=interaction-row]").get(i).select("span").get(spansize - 1)
						.text();
				program += (isNum(proget));

			}
			else if (rows.indexOf("附加题") != -1) {
				String addget = smallDoc.select("div[class=interaction-row]").get(i).select("span").get(spansize - 1)
						.text();
				add += (isNum(addget));
			}
		}
		// 大网页经验值计算
		// 统计大网页类
		int rowslength1 = allDoc.select("div[class=interaction-row]").size();
		for (int i = 0; i < rowslength1; i++) {
			int rowssizea = allDoc.select("div[class=interaction-row]").get(i).select("span").size();
			String rowsa = allDoc.select("div[class=interaction-row]").get(i).select("span").get(1).text();
			int spansize = allDoc.select("div[class=interaction-row]").get(i).select("span").size();
			// 统计课前自测经验值的相加
			if (rowsa.indexOf("课前自测") != -1) {
				String selfget = allDoc.select("div[class=interaction-row]").get(i).select("span").get(spansize - 1)
						.text();
				before += isNum(selfget);
			}
		}
		// 根据成绩要求哦完成计算
		double before1 = before / (double) total_before * 100;
		double base1 = base / (float) total_base * 100 * 0.95;
		double test1 = test / (double) total_test * 100;
		double program1 = program / total_program * 100;
		if (program1 >= 95.0)
			program1 = 95.0;
		double add1 = add / total_add * 100;
		if (add1 >= 90.0)
			add1 = 90.0;
		double last_score = before1 * 0.25 + base1 * 0.3 + test1 * 0.2 + program1 * 0.1 + add1 * 0.05+6;
		String result = String.format("%.1f", last_score);
		System.out.println(result);}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//将获取的经验值从字符串类型转换为double类型，同时删去获取span里的文字
	public static int isNum(String str) {
		String str1 = "";
		for (int i = 0; i < str.length(); i++) {
			char chr = str.charAt(i);
			if (chr >= 48 && chr <= 57)
				str1 += chr;
		}
		int m = Integer.parseInt(str1);
		return m;
	}
}
